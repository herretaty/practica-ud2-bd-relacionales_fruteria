package base.enums;

public enum NombreFrutas {
    MANZANA("Manzana"),
    PERA("Pera"),
    MANGO("Mango"),
    MARACUYA("Maracuya"),
    MELOCOTON("Melocoton"),
    MELON("Melon"),
    PIÑA("Piña"),
    PLATANO("Platano"),
    POMELO("Pomelo"),
    UVA("Uva"),
    HIGO("Higo"),
    KIWI("Kiwi"),
    LIMA("Lima"),
    LIMON("Limon"),
    MANDARINA("Mandarina"),
    MORO("Mora"),
    ARANDANO("Arandano"),
    CEREZA("Cereza"),
    FRESA("Fresa"),
    COCO("Coco"),
    FRAMBUESA("Frambuesa"),
    ALBARICOQUE("Albaricoque"),
    CIRUELA("Ciruela"),
    NARANJA("Naranja");

    private String valor;

    NombreFrutas(String valor){
        this.valor= valor;
    }

    public String getValor(){
        return valor;
    }






}
