package base.enums;

public enum TiposOrigen {
    ARAGON("Aragon"),
    MURCIA("Murcia"),
    ITALIA("Italia"),
    NUEVA_ZELANDA("Nueva_Zelanda"),
    VALENCIA("VALENCIA"),
    ISLAS_CANARIAS("Islas_Canarias"),
    ALICANTE("Alicante"),
    COSTA_RICA("Costa_Rica"),
    BRASIL("Brasil");

    private String valor;

    TiposOrigen(String valor){
        this.valor= valor;
    }

    public String getValor(){
        return valor;
    }

}
