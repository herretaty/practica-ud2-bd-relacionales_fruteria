package gui;

import util.Util;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.event.*;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;

public class Controlador implements ActionListener, ItemListener, ListSelectionListener, WindowListener {
    private Modelo modelo;
    private Vista vista;
    boolean refrescar;

    public Controlador(Modelo modelo, Vista vista) {
        this.modelo = modelo;
        this.vista = vista;
        modelo.conectar();
        setOptions();
        addActionListeners(this);
        addItemListeners(this);
        addWindowListeners(this);
        refrescarTodo();
    }

    private void refrescarTodo() {
        refrescarLugar();
        refrescarOrigen();
        refresarFruta();

        refrescar = false;
    }

    private void addActionListeners(ActionListener listener) {
        vista.btnFrutaAnadir.addActionListener(listener);
        vista.btnFrutaModificar.addActionListener(listener);
        vista.btnFrutaEliminar.addActionListener(listener);

        vista.btnOrigenAnadir.addActionListener(listener);
        vista.btnOrigenModificar.addActionListener(listener);
        vista.btnOrigenEliminar.addActionListener(listener);

        vista.btnLugarAnadir.addActionListener(listener);
        vista.btnLugarModificar.addActionListener(listener);
        vista.btnlugarEliminar.addActionListener(listener);

        vista.optionDialog.btnOpcionesGuardar.addActionListener(listener);
        vista.itemOpciones.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.btnValidate.addActionListener(listener);
    }

    private void addWindowListeners(WindowListener listener) {
        vista.addWindowListener(listener);
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting() && !((ListSelectionModel) e.getSource()).isSelectionEmpty()) {
            if (e.getSource().equals(vista.lugarTabla.getSelectionModel())) {
                int row = vista.lugarTabla.getSelectedRow();
                vista.txtDireccion.setText(String.valueOf(vista.lugarTabla.getValueAt(row, 1)));
                vista.txtTelefono.setText(String.valueOf(vista.lugarTabla.getValueAt(row, 2)));
                vista.txtCiudad.setText(String.valueOf(vista.lugarTabla.getValueAt(row, 3)));
                vista.txtWeb.setText(String.valueOf(vista.lugarTabla.getValueAt(row, 4)));
                vista.comboTipoOrigen.setSelectedItem(String.valueOf(vista.lugarTabla.getValueAt(row, 5)));
            } else if (e.getSource().equals(vista.origenTabla.getSelectionModel())) {
                int row = vista.origenTabla.getSelectedRow();
                vista.txtLugar.setText(String.valueOf(vista.origenTabla.getValueAt(row, 1)));
                vista.txtDesarrollo.setText(String.valueOf(vista.origenTabla.getValueAt(row, 2)));
                vista.txtTemporada.setText(String.valueOf(vista.origenTabla.getValueAt(row, 3)));
                vista.fechaEntrega.setDate((Date.valueOf(String.valueOf(vista.origenTabla.getValueAt(row, 4)))).toLocalDate());
            } else if (e.getSource().equals(vista.frutaTabla.getSelectionModel())) {
                int row = vista.frutaTabla.getSelectedRow();
                vista.txtTipo.setText(String.valueOf(vista.frutaTabla.getValueAt(row, 1)));
                vista.comboNombre.setSelectedItem(String.valueOf(vista.frutaTabla.getValueAt(row, 2)));
                vista.comboOrigen.setSelectedItem(String.valueOf(vista.frutaTabla.getValueAt(row, 3)));
                vista.comboFrutas.setSelectedItem(String.valueOf(vista.frutaTabla.getValueAt(row, 4)));
                vista.txtCaracteristicas.setText(String.valueOf(vista.frutaTabla.getValueAt(row, 5)));
                vista.txtVitaminas.setText(String.valueOf(vista.frutaTabla.getValueAt(row, 6)));
                vista.txtColor.setText(String.valueOf(vista.frutaTabla.getValueAt(row, 7)));
                vista.txtPrecio.setText(String.valueOf(vista.frutaTabla.getValueAt(row, 8)));
                vista.fechaCaducidad.setDate(Date.valueOf(String.valueOf(vista.frutaTabla.getValueAt(row, 9))).toLocalDate());
            } else if (e.getValueIsAdjusting()
                    && ((ListSelectionModel) e.getSource()).isSelectionEmpty() && !refrescar) {
                if (e.getSource().equals(vista.lugarTabla.getSelectionModel())) {
                    borrarCampoLugar();
                } else if (e.getSource().equals(vista.origenTabla.getSelectionModel())) {
                    borrarCampoOrigen();
                } else if (e.getSource().equals(vista.frutaTabla.getSelectionModel())) {
                    borrarCampoFruta();
                }

            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        switch (command) {
            case "Opciones":
                vista.adminPasswordDialog.setVisible(true);
                break;
            case "Desconectar":
                modelo.desconectar();
                break;
            case "Salir":
                System.exit(0);
                break;
            case "abrirOpciones":
                if (String.valueOf(vista.adminPassword.getPassword()).equals(modelo.getAdminPassword())) {
                    vista.adminPassword.setText("");
                    vista.adminPasswordDialog.dispose();
                    vista.optionDialog.setVisible(true);
                } else {
                    Util.showErrorAlert("La contraseña introducida no es correcta.");
                }
                break;
            case "guardarOpciones":
                modelo.setPropValues(vista.optionDialog.tfIP.getText(), vista.optionDialog.tfUser.getText(),
                        String.valueOf(vista.optionDialog.pfPass.getPassword()), String.valueOf(vista.optionDialog.pfAdmin.getPassword()));
                vista.optionDialog.dispose();
                vista.dispose();
                new Controlador(new Modelo(), new Vista());
                break;
            case "añadirFruta": {
                try {
                    if (comprobarFrutaVacia()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.frutaTabla.clearSelection();
                    } else if (modelo.frutaTipoYaExiste(vista.txtTipo.getText())) {
                        Util.showErrorAlert("Ese tipo de fruta ya existe.\n Introduce una fruta diferente");
                        vista.frutaTabla.clearSelection();
                    } else {
                        modelo.insertarFruta (
                                vista.txtTipo.getText(),
                                String.valueOf(vista.comboNombre.getSelectedItem()),
                                String.valueOf(vista.comboOrigen.getSelectedItem()),
                                String.valueOf(vista.comboFrutas.getSelectedItem()),
                                vista.txtCaracteristicas.getText(),
                                vista.txtVitaminas.getText(),
                                vista.txtColor.getText(),
                                Float.parseFloat(vista.txtPrecio.getText()),
                                vista.fechaCaducidad.getDate());
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce numeros en los campos que lo requieren");
                    vista.frutaTabla.clearSelection();
                }
                borrarCampoFruta();
                refresarFruta();
            }
            break;
            case "modificarFruta":{
                try {
                    if (comprobarFrutaVacia()){
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.frutaTabla.clearSelection();
                    }else{
                        modelo.modificarFruta(
                                vista.txtTipo.getText(),
                                String.valueOf(vista.comboNombre.getSelectedItem()),
                                String.valueOf(vista.comboOrigen.getSelectedItem()),
                                String.valueOf(vista.comboFrutas.getSelectedItem()),
                                vista.txtCaracteristicas.getText(),
                                vista.txtVitaminas.getText(),
                                vista.txtColor.getText(),
                                Float.parseFloat(vista.txtPrecio.getText()),
                                vista.fechaCaducidad.getDate(),
                                Integer.parseInt((String)vista.frutaTabla.getValueAt(vista.frutaTabla.getSelectedRow(),0)));
                        
                    }
                }catch (NumberFormatException nfe){
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.frutaTabla.clearSelection();
                }
                borrarCampoFruta();
                refresarFruta();
            }
            break;
            case "eliminarFruta":
                modelo.borrarFruta(Integer.parseInt((String)vista.frutaTabla.getValueAt(vista.frutaTabla.getSelectedRow(),0)));
                borrarCampoFruta();
                refresarFruta();
                break;
            case "anadirOrigen":{
                try{
                    if (comprobarOrigenVacio()){
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.origenTabla.clearSelection(); 
                    }else if (modelo.origenLugarYaExiste(vista.txtLugar.getText(),
                            vista.txtDesarrollo.getText())){
                        Util.showErrorAlert("Ese lugar ya existe.\n Imtroduce un lugar diferente");
                        vista.origenTabla.clearSelection();
                    }else{
                        modelo.insertarOrigen(vista.txtLugar.getText(),
                       vista.txtDesarrollo.getText(), 
                       vista.txtTemporada.getText());
                        refrescarOrigen(); 
                    }
                }catch (NumberFormatException nfe){
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.origenTabla.clearSelection();
                }
                borrarCampoOrigen();
            }
            break;
            case "modificarOrigen":{
                try {
                    if (comprobarOrigenVacio()){
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.origenTabla.clearSelection();
                    }else {
                       modelo.modificarOrigen(vista.txtLugar.getText(),vista.txtDesarrollo.getText(),
                               vista.txtTemporada.getText(),vista.fechaCaducidad.getDate(),
                               Integer.parseInt((String)vista.origenTabla.getValueAt(vista.origenTabla.getSelectedRow(),0)));
                       refrescarOrigen();
                    }
                }catch (NumberFormatException nfe){
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.origenTabla.clearSelection();
                }
                borrarCampoOrigen();
            }
            break;
            case "eliminarOrigen":
                modelo.borrarOrigen(Integer.parseInt((String)vista.origenTabla.getValueAt(vista.origenTabla.getSelectedRow(),0)));
                borrarCampoOrigen();
                refrescarOrigen(); 
                break;
            case "añadirLugar":{
                try {
                    if (comprobarLugarVacio()){
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.lugarTabla.clearSelection();
                    }else if (modelo.lugarDireccionYaExiste(vista.txtDireccion.getText())){
                       Util.showErrorAlert("Ese nombre ya existe.\nIntroduce una direccion diferente"); 
                       vista.lugarTabla.clearSelection();
                    }else {
                        modelo.insertarLugar(vista.txtDireccion.getText(),vista.txtTelefono.getText(),
                                vista.txtCiudad.getText(),vista.txtWeb.getText(),(String)vista.comboTipoOrigen.getSelectedItem());
                        refrescarLugar();
                    }
                }catch (NumberFormatException nfe){
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.lugarTabla.clearSelection();
                }
                borrarCampoLugar();
            }
            break;
            case"modificarLugar":{
                try {
                    if (comprobarLugarVacio()){
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.lugarTabla.clearSelection();
                    }else {
                        modelo.modificarLugar(vista.txtDireccion.getText(),vista.txtTelefono.getText(),vista.txtCiudad.getText(),
                                vista.txtWeb.getText(),String.valueOf(vista.comboTipoOrigen.getSelectedItem()),
                                Integer.parseInt((String)vista.lugarTabla.getValueAt(vista.lugarTabla.getSelectedRow(),0)));
                        refrescarLugar();
                    }
                }catch (NumberFormatException nfe){
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.lugarTabla.clearSelection();
                }
                borrarCampoLugar();
            }
            break;
            case "eliminarLugar":
                modelo.borrarLugar(Integer.parseInt((String)vista.lugarTabla.getValueAt(vista.lugarTabla.getSelectedRow(),0)));
                borrarCampoLugar();
                refrescarLugar();
                break;
        }
        
    }

    private void refrescarLugar() {
        try {
            vista.lugarTabla.setModel(construirTableModelLugar(modelo.consultaLugar()));
            vista.comboNombre.removeAllItems();
            for (int i= 0; i<vista.dtmLugar.getRowCount(); i++){
               vista.comboNombre.addItem(vista.dtmLugar.getValueAt(i, 0)+" - "+
                       vista.dtmLugar.getValueAt(i,1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private DefaultTableModel construirTableModelLugar(ResultSet cl) throws SQLException {
        ResultSetMetaData metaData= cl.getMetaData();

        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(cl, columnCount, data);
        vista.dtmLugar.setDataVector(data,columnNames);

        return vista.dtmLugar;
    }

    private void setDataVector(ResultSet cl, int columnCount, Vector<Vector<Object>> data) throws SQLException {
        while (cl.next()){
            Vector<Object> vector= new Vector<>();
            for (int columIndex= 1; columIndex <=columnCount; columIndex++){
                vector.add(cl.getObject(columIndex));
            }
            data.add(vector);
        }
    }

    private boolean comprobarLugarVacio() {
        return vista.txtDireccion.getText().isEmpty() ||
        vista.txtTelefono.getText().isEmpty() ||
        vista.txtCiudad.getText().isEmpty() ||
        vista.txtWeb.getText().isEmpty() ||
        vista.comboTipoOrigen.getSelectedIndex()==1;

    }

    private boolean comprobarOrigenVacio() {
        return vista.txtLugar.getText().isEmpty()||
        vista.txtDesarrollo.getText().isEmpty()||
        vista.txtTemporada.getText().isEmpty()||
        vista.fechaEntrega.getText().isEmpty();
    }

    private void refresarFruta() {
        try {
            vista.frutaTabla.setModel(construirTableModelFruta(modelo.consultarFrutas()));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private DefaultTableModel construirTableModelFruta(ResultSet cf) throws SQLException {
        ResultSetMetaData metaData = cf.getMetaData();

        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(cf, columnCount, data);

        vista.dtmFruta.setDataVector(data,columnNames);
        return vista.dtmFruta;

    }

    private boolean comprobarFrutaVacia() {
        return vista.txtTipo.getText().isEmpty() ||
        vista.txtCaracteristicas.getText().isEmpty() ||
        vista.txtVitaminas.getText().isEmpty() ||
        vista.txtColor.getText().isEmpty() ||
        vista.txtPrecio.getText().isEmpty() ||
        vista.comboNombre.getSelectedIndex()== -1 ||
        vista.comboOrigen.getSelectedIndex()== -1 ||
        vista.comboFrutas.getSelectedIndex()== -1 ||
        vista.fechaCaducidad.getText().isEmpty();
    }

    private void borrarCampoFruta() {
        vista.comboNombre.setSelectedIndex(-1);
        vista.comboOrigen.setSelectedIndex(-1);
        vista.comboFrutas.setSelectedIndex(-1);
        vista.txtTipo.setText("");
        vista.txtCaracteristicas.setText("");
        vista.txtVitaminas.setText("");
        vista.txtColor.setText("");
        vista.txtPrecio.setText("");
        vista.fechaCaducidad.setText("");
    }

    private void borrarCampoOrigen() {
        vista.txtLugar.setText("");
        vista.txtDesarrollo.setText("");
        vista.txtTemporada.setText("");
        vista.fechaEntrega.setText("");
    }

    private void borrarCampoLugar() {
        vista.txtDireccion.setText("");
        vista.txtTelefono.setText("");
        vista.txtCiudad.setText("");
        vista.txtWeb.setText("");
        vista.comboTipoOrigen.setSelectedIndex(-1);
    }


    private void refrescarOrigen() {
        try{
            vista.origenTabla.setModel(construirTableModelOrigen(modelo.consultarOrigen()));
            vista.comboOrigen.removeAllItems();
            for (int i=0; i<vista.dtmOrigen.getRowCount();i++){
                vista.comboOrigen.addItem(vista.dtmOrigen.getValueAt(i,0)+"-"+
                        vista.dtmOrigen.getValueAt(i,2)+" , "+ vista.dtmOrigen.getValueAt(i,1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private DefaultTableModel construirTableModelOrigen(ResultSet co) throws SQLException {
        ResultSetMetaData metaData = co.getMetaData();

        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(co, columnCount, data);

        vista.dtmOrigen.setDataVector(data,columnNames);
        return vista.dtmOrigen;

    }

    @Override
    public void windowClosing(WindowEvent e) {
        System.exit(0);
    }

    private void setOptions() {
        vista.optionDialog.tfIP.setText(modelo.getIP());
        vista.optionDialog.tfUser.setText(modelo.getUser());
        vista.optionDialog.pfPass.setText(modelo.getPassword());
        vista.optionDialog.pfAdmin.setText(modelo.getAdminPassword());
    }

    private void addItemListeners(ActionListener listener) {
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }
}
