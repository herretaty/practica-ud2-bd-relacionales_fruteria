package gui;

import java.io.*;
import java.sql.*;
import java.time.LocalDate;
import java.util.Properties;

public class Modelo {
    private String ip;
    private String user;
    private String password;
    private String adminPassword;

    public Modelo() {
        getPropValues();
    }

    String getIP() {
        return ip;
    }
    String getUser() {
        return user;
    }
    String getPassword() {
        return password;
    }
    String getAdminPassword() {
        return adminPassword;
    }
    private Connection connection;

    void conectar() {
        try {
            connection = DriverManager.getConnection(
                    "jdbc:mysql://"+ip+":3306/mibase",user, password);
        } catch (SQLException sqle) {
            try {
                connection = DriverManager.getConnection(
                        "jdbc:mysql://"+ip+":3306/",user, password);

                PreparedStatement statement = null;

                String code= leerFichero();
                String[] query = code.split("--");
                for (String aQuery : query) {
                    statement = connection.prepareStatement(aQuery);
                    statement.executeUpdate();
                }
                assert statement != null;
                statement.close();

            } catch (SQLException | IOException e) {
                e.printStackTrace();
            }
        }
    }

    private String leerFichero() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader("basedatos_java.sql"))) {
            String linea;
            StringBuilder stringBuilder = new StringBuilder();
            while ((linea = reader.readLine()) != null) {
                stringBuilder.append(linea);
                stringBuilder.append(" ");
            }

            return stringBuilder.toString();
        }
    }

    void desconectar() {
        try {
            connection.close();
            connection = null;
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
    }

    void insertarFruta(String text, String tipo, String caracteristicas, String origen, String lugar, String vitaminas, String color,
                       float precio, LocalDate fechaCaducidad){
        String sentenciaSql= "INSERT INTO frutas(tipo, caracteristicas, vitaminas, color, precio, fechaCaducidad)VALUES(?,?,?,?,?,?";
        PreparedStatement sentencia= null;

        int idorigen= Integer.valueOf(origen.split("")[0]);
        int idlugar= Integer.valueOf(lugar.split("")[0]);
        try {
            sentencia= connection.prepareStatement(sentenciaSql);
            sentencia.setString(1,tipo);
            sentencia.setString(2,caracteristicas);
            sentencia.setString(3,vitaminas);
            sentencia.setString(4,color);
            sentencia.setFloat(5,precio);
            sentencia.setDate(6,Date.valueOf(fechaCaducidad));
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }finally {
            if (sentencia!= null)
                try {
                    sentencia.close();
                }catch (SQLException sqle){
                    sqle.printStackTrace();
                }
        }
    }

    void insertarOrigen(String lugar, String desarrollo, String temporada){
        String sentenciaSql= "INSERTAR INTO origen(lugar, desarrollo, temporada)VALUES(?,?,?)";
        PreparedStatement sentencia= null;
        try {
            sentencia= connection.prepareStatement(sentenciaSql);
            sentencia.setString(1,lugar);
            sentencia.setString(2,desarrollo);
            sentencia.setString(3,temporada);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }finally {
            if (sentencia!= null)
                try {
                    sentencia.close();
                }catch (SQLException sqle){
                    sqle.printStackTrace();
                }
        }
    }

    void insertarLugar(String text, String direccion, String telefono, String ciudad, String web){
        String sentenciaSql= "INSERT INTO lugar(direccion, telefono, ciudad, web)VALUES(?,?,?,?)";
        PreparedStatement sentencia= null;
        try {
            sentencia= connection.prepareStatement(sentenciaSql);
            sentencia.setString(1,direccion);
            sentencia.setString(2,telefono);
            sentencia.setString(3,ciudad);
            sentencia.setString(4,web);
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }finally {
            if (sentencia!= null)
                try {
                    sentencia.close();
                }catch (SQLException sqle){
                    sqle.printStackTrace();
                }
        }
    }

    void modificarFruta(String tipo, String caracteristicas, String origen, String lugar, String vitaminas, String color,
                        String text, Float precio, LocalDate fechaCaducidad, int idfruta){
       String sententeciaSql="UPDATE frutas SET tipo=?, caracteristicas=?, idorigen=?, idlugar=?, vitaminas=?, color=?" +
               "precio=?, fechaCaducidad=?, WHERE idfruta=?";
       PreparedStatement sentencia= null;

        int idorigen= Integer.valueOf(origen.split("")[0]);
        int idlugar= Integer.valueOf(lugar.split("")[0]);
        try {
            sentencia= connection.prepareStatement(sententeciaSql);
            sentencia.setString(1,tipo);
            sentencia.setString(2,caracteristicas);
            sentencia.setString(3,vitaminas);
            sentencia.setString(4,color);
            sentencia.setFloat(5,precio);
            sentencia.setDate(6,Date.valueOf(fechaCaducidad));
            sentencia.setInt(8,idfruta);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }finally {
            if (sentencia!= null)
                try {
                    sentencia.close();
                }catch (SQLException sqle){
                    sqle.printStackTrace();
                }
        }
    }

    void modificarOrigen(String lugar, String desarrollo, String temporada,LocalDate fechaEntrega, int idorigen){
        String sententeciaSql="UPDATE origenes SET lugar=?, desarrollo=?, temporada=?," +
                "WHERE idorigen";
        PreparedStatement sentencia= null;
        try {
            sentencia= connection.prepareStatement(sententeciaSql);
            sentencia.setString(1,lugar);
            sentencia.setString(2,desarrollo);
            sentencia.setString(3,temporada);
            sentencia.setDate(4,Date.valueOf(fechaEntrega));
            sentencia.setInt(5,idorigen);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }finally {
            if (sentencia!= null)
                try {
                    sentencia.close();
                }catch (SQLException sqle){
                    sqle.printStackTrace();
                }
        }
    }

    void modificarLugar(String direccion, String telefono, String ciudad, String web, String s, int idlugar){
        String sentenciaSql= "UPDATE lugar SET direccion=?, telefono=?, ciudad=?, web=?)" +
                "WHERE idlugar=?";
        PreparedStatement sentencia= null;
        try {
            sentencia= connection.prepareStatement(sentenciaSql);
            sentencia.setString(1,direccion);
            sentencia.setString(2,telefono);
            sentencia.setString(3,ciudad);
            sentencia.setString(4,web);
            sentencia.setInt(5,idlugar);
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }finally {
            if (sentencia!= null)
                try {
                    sentencia.close();
                }catch (SQLException sqle){
                    sqle.printStackTrace();
                }
        }
    }
    void borrarFruta(int idfruta){
        String sentenciaSql= "DELETE FROM frutas WHERE id fruta=?";
        PreparedStatement sentencia= null;
        try {
            sentencia= connection.prepareStatement(sentenciaSql);
            sentencia.setInt(1,idfruta);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }finally {
            if (sentencia!=null)
                try {
                    sentencia.close();
                }catch (SQLException sqle){
                    sqle.printStackTrace();
                }
        }

    }

    void borrarOrigen(int idorigen){
        String sentenciaSql= "DELETE FRON origenes WHERE idorigen=?";
        PreparedStatement sentencia= null;
        try {
            sentencia= connection.prepareStatement(sentenciaSql);
            sentencia.setInt(1,idorigen);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }finally {
            if (sentencia!=null)
                try {
                    sentencia.close();
                }catch (SQLException sqle){
                    sqle.printStackTrace();
                }
        }
    }

    void borrarLugar(int idlugar){
        String sentenciaSql= "DELETE FROM lugares WHERE idlugar=?";
        PreparedStatement sentencia= null;
        try {
            sentencia= connection.prepareStatement(sentenciaSql);
            sentencia.setInt(1,idlugar);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }finally {
            if (sentencia!= null)
                try {
                    sentencia.close();
                }catch (SQLException sqle){
                    sqle.printStackTrace();
                }
        }
    }

    ResultSet consultarFrutas()throws SQLException{
        String sentenciaSql= "SELECT concat(b.idfruta) as 'ID',concat(b.tipo) as 'TIPO', concat(b.caracteristicas) as 'CARACTERISTICAS," +
         "concat(e.idorigen, '_', e.origen) as 'EDITORIAL', concat(b.vitamina) as 'VITAMINA" +
          "concat(a.idlugar '_', a.direccion, ',', a.telefono) as 'LUGAR',"+
                "concat(b.precio) as 'PRECIO', concat(b.fechaCaducidad) as 'FECHA'"+
                "FROM frutas as b"+
                "inner join lugares as e on e.idlugar= b.ideditorial inner join"+
                "origenes as a on a.idirigenes= b.idorigen";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia= connection.prepareStatement(sentenciaSql);
        resultado= sentencia.executeQuery();
        return resultado;
    }

    ResultSet consultarOrigen()throws SQLException{
        String sentenciaSql= "SELECT concat(idorigen) as 'ID', concat(lugar) as 'LUGAR', concat(desarrollo) as 'DESARROLLO',"+
          "concat(fechaEntrega) as 'FECHA DE ENTREGA', concat(temporada) as 'TEMPORADA', FROM origenes";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = connection.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    ResultSet consultaLugar() throws SQLException{
        String sentenciaSql= "SELECT concat(idlugar) as 'ID', concat(direccion) as, 'DIRECCION FRUTERIA', concat(telefono) as 'TELEFONO',"+
          "concat(ciudad) as 'CIUDAD', concat(tipoorigen) as 'TIPO' concant(web) as 'WEB' FROM lugares";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = connection.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    private void getPropValues() {
        InputStream inputStream = null;
        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";

            inputStream = new FileInputStream(propFileName);

            prop.load(inputStream);
            ip = prop.getProperty("ip");
            user = prop.getProperty("user");
            password = prop.getProperty("pass");
            adminPassword = prop.getProperty("admin");

        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            try {
                if (inputStream != null) inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    void setPropValues(String ip, String user, String pass, String adminPass) {
        try {
            Properties prop = new Properties();
            prop.setProperty("ip", ip);
            prop.setProperty("user", user);
            prop.setProperty("pass", pass);
            prop.setProperty("admin", adminPass);
            OutputStream out = new FileOutputStream("config.properties");
            prop.store(out, null);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        this.ip = ip;
        this.user = user;
        this.password = pass;
        this.adminPassword = adminPass;
    }

    public boolean frutaTipoYaExiste(String tipo){
        String tipoConsult= "SELECT existeTipo(?)";
        PreparedStatement function;
        boolean tipoExists= false;
        try {
            function= connection.prepareStatement(tipoConsult);
            function.setString(1,tipo);
            ResultSet resultSet= function.executeQuery();
            resultSet.next();
            tipoExists= resultSet.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tipoExists;
    }

    public boolean lugarDireccionYaExiste(String direccion){
        String lugarAddressConsult= "SELECT existeDireccionLugar(?)";
        PreparedStatement function;
        boolean addressExists= false;
        try {
            function= connection.prepareStatement(lugarAddressConsult);
            function.setString(1,direccion);
            ResultSet resultSet= function.executeQuery();
            resultSet.next();
            addressExists= resultSet.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return addressExists;
    }

    public boolean origenLugarYaExiste(String lugar, String desarrollo){
        String place= desarrollo + " , "+ lugar;
        String originPlaceConsult= "SELECT existeLugarOrigen(?)";
        PreparedStatement function;
        boolean addressExists= false;
        try {
            function= connection.prepareStatement(originPlaceConsult);
            function.setString(1,place);
            ResultSet resultSet= function.executeQuery();
            resultSet.next();
            addressExists= resultSet.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return addressExists;
    }
}

