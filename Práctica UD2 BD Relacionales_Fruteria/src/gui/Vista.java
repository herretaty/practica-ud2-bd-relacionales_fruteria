package gui;

import base.enums.NombreFrutas;
import base.enums.TiposOrigen;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class Vista extends JFrame {
    private final static String TITULOFRAME= "Fruteria";
    private JPanel panel1;
    private JTabbedPane tabbedPane;


    /*LUGARFRUTERIA*/
    JTextField txtDireccion;
    JTextField txtTelefono;
    JTextField txtCiudad;
    JTextField txtWeb;
    JTable lugarTabla;
    JButton btnLugarAnadir;
    JButton btnLugarModificar;
    JButton btnlugarEliminar;
    JComboBox<String>comboTipoOrigen;

    /*ORIGEN*/
    JTextField txtLugar;
    JTextField txtDesarrollo;
    JTextField txtTemporada;
    JTable origenTabla;
    JButton btnOrigenAnadir;
    JButton btnOrigenModificar;
    JButton btnOrigenEliminar;
    DatePicker fechaEntrega;


    /*FRUTA*/
    JTextField txtTipo;
    JComboBox<String>comboNombre;
    JComboBox<String>comboOrigen;
    JComboBox<String>comboFrutas;
    JTextField txtCaracteristicas;
    JTextField txtVitaminas;
    JTextField txtColor;
    JTextField txtPrecio;
    JTable frutaTabla;
    JButton btnFrutaAnadir;
    JButton btnFrutaModificar;
    JButton btnFrutaEliminar;
    DatePicker fechaCaducidad;

    /*SEARCH*/
    JLabel estadoFruta;

    /*DEFAULT TABLE MODELS*/
    DefaultTableModel dtmOrigen;
    DefaultTableModel dtmFruta;
    DefaultTableModel dtmLugar;
    
    /*MENU*/
    JMenuItem itemOpciones;
    JMenuItem itemDesconectar;
    JMenuItem itemSalir;

    /*OPTION DIALOG*/
    OptionDialog optionDialog;
    
    /*SAVECHANGESDIALOG*/
    JDialog adminPasswordDialog;
    JButton btnValidate;
    JPasswordField adminPassword;

    public Vista() {
        super(TITULOFRAME);
        initFrame();
    }

    private void initFrame() {
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        this.setSize(new Dimension(this.getWidth()+200, this.getHeight()+100));
        this.setLocationRelativeTo(null);
        optionDialog = new OptionDialog(this);
        setMenu();
        setAdminDialog();
        setEnumComboBox();
        setTableModels();
    }

    private void setTableModels() {
        this.dtmOrigen= new DefaultTableModel();
        this.origenTabla.setModel(dtmOrigen);

        this.dtmFruta= new DefaultTableModel();
        this.frutaTabla.setModel(dtmFruta);

        this.dtmLugar= new DefaultTableModel();
        this.lugarTabla.setModel(dtmLugar);


    }



    private void setEnumComboBox() {
        for (TiposOrigen constant: TiposOrigen.values()){
           comboTipoOrigen.addItem(constant.getValor());
        }comboTipoOrigen.setSelectedIndex(-1);

        for (NombreFrutas constant: NombreFrutas.values()){
            comboFrutas.addItem(constant.getValor());
        }comboFrutas.setSelectedIndex(-1);
    }

    private void setAdminDialog() {
        btnValidate = new JButton("Validar");
        btnValidate.setActionCommand("abrirOpciones");
        adminPassword = new JPasswordField();
        adminPassword.setPreferredSize(new Dimension(100, 26));
        Object[] options = new Object[] {adminPassword, btnValidate};
        JOptionPane jop = new JOptionPane("Introduce la contraseña"
                , JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION, null, options);

        adminPasswordDialog = new JDialog(this, "Opciones", true);
        adminPasswordDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        adminPasswordDialog.setContentPane(jop);
        adminPasswordDialog.pack();
        adminPasswordDialog.setLocationRelativeTo(this);
    }

    private void setMenu() {
        JMenuBar mbBar = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        itemOpciones = new JMenuItem("Opciones");
        itemOpciones.setActionCommand("Opciones");
        itemDesconectar = new JMenuItem("Desconectar");
        itemDesconectar.setActionCommand("Desconectar");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");
        menu.add(itemOpciones);
        menu.add(itemDesconectar);
        menu.add(itemSalir);
        mbBar.add(menu);
        mbBar.add(Box.createHorizontalGlue());
        this.setJMenuBar(mbBar);
    }


}
